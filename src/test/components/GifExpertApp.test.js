import { shallow } from "enzyme";
import { GifExpertApp } from "../../components/GifExpertApp";

describe("Test suite for GifExpertApp component", () => {
  test("should render component correctly", () => {
    const wrapper = shallow(<GifExpertApp />);
    expect(wrapper).toMatchSnapshot();
  });

  test("should show a list of categories", () => {
    const categories = ["DBZ", "Naruto"];
    const wrapper = shallow(<GifExpertApp defaultCategories={categories} />);
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find("GifGrid").length).toBe(categories.length);
  });
});
